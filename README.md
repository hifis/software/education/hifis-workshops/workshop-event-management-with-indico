---
tags: #indico #workshop
title: Event management with Indico
---

Test

# Sessions

See [Link](episodes/lecture-getting-started-with-indico.md)


# Reminder Email via Indico

Please replace the markdown syntax with internal Indico editor.

~~~
Dear {first_name},

this is a reminder for the upcoming workshop **{event_title}**.
We will meet at 9 am on Tuesday, the 15.11.2022 online.

- Link to [the video conferencing platform gather.town][videoconf]
- Link to [the collaborative document for the Indico workshop][pad]

You can view your registration details on this page:
{link}

Best regards
Thomas Foerster

<!-- Links -->
[videoconf]: https://app.gather.town/app/6MD2QpxwnwQairtX/Workshop
[pad]: https://notes.desy.de/ONGuBmp_TZib1Dh5SZT0QA#
~~~

