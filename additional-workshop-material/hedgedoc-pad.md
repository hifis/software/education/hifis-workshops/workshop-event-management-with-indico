---
tags: workshop, indico, 
title: Getting started with Indico
---

----

<p style="margin-bottom: 2em;"></p>

<figure>
<a href="https://hifis.net/">
<img src="https://s3.desy.de/hackmd/uploads/upload_7d9dbb573e06bfda5cf2fea779ff02f1.png"
     alt="HIFIS Logo"
     width=60%; style="background:none; border:none; box-shadow:none;">
    <figcaption style="font-size:1.0em; color:#005AA0">
        <p style="margin-bottom: 0.5em;"></p> 
        <b>Helmholtz Digital Services for Science — Collaboration made easy.</b>
    </figcaption>
</a>
</figure>

----

<p style="margin-bottom: 2em;"></p>


# <p style="color: #005AA0; text-align: center; margin-bottom: -0.2em">Getting started with <img width=15% src="https://s3.desy.de/hackmd/uploads/upload_61d6930468515ad9db9825294f75878c.png" alt="Indico"> </p>

::: warning
This document serves as a collective memory during the course.
Your instructor will guide you through, so don't worry about having to read it all right now :smile: 
:::

::: info
## Short Overview

**Before we start I would like to give some information, how the course is structured.**

| Lesson                            | Time   | 
| --------------------------------- | ------ | 
| (1) An Introduction to Indico     | 90 min | 
| (2) Hands on Excercise            | 90 min | 
| (3) Questions/Discussion          | 15 min | 

- It is a hands on course, meaning:
    - I will give a brief introduction, how to work with Indico, in the first season
    - And next we will work in groups of up to 3 people to build our first course
    - Finally in the last 30 min, we would like that we give each other feedback 

- Course material is shared via a [HedgeDoc Pad][hedgedoc-pad]
    - Link provided via email, I will put it in the chat as well.
    - Be aware, the material function as collective memory
    - It is interactive and summarises the main information

- Course video conference is held on [Gather][gather]. Link will be send via Email.


## Code of Conduct

We adhere to the [Software Carpentries' Code of Conduct][coc-carp-doc].

You can report violations to

- The Instructors and Helpers
- The [:email: HIFIS helpdesk][coc-helpdesk]
- The [Software Carpentries][coc-carp]


## Contact

- [:email: HIFIS support](mailto:support@hifis.net)
- [:email: Thomas Förster](mailto:thomas.foerster@hzdr.de)
- [HIFIS consulting inquiry](https://hifis.net/consulting)

:::

# Introduction

## Let's have s short introduction

::: info

Please add your name, what you are working on and what course your would like to promote in the future, also non-technical topics (*Drawing the best cat pics*).

| Abr. | Name           | My work/research?  | What course to organize?             |
| ---- | ------         | ----------         | ----------                           |
| TF   | Thomas Förster | RSE and Consultant | Gitlab CI                            |
| FE   | Fredo Erxleben | RSE and Trainer    | Drawing cats :cat: with water colors |

:::

## Background and helpful links about Indico

[Indico][indico] is a self-hosted web-based event management tool and actively developed by CERN. 

> The effortless open-source tool for event organisation, archival and collaboration 

- [Helmholtz (HIFIS) host an instance of Indico][events.hifis.net]. 
- User documentation can be found on [learn.getindico.io] 




:::info
### Indico Documentation
[Find the Indico documentation here][indico-doc]
:::


# Questions / Discussion

::: info
**[Post Survey][survey]**
:::

# Afflication

- <hifis.net>
- [HIFIS Consulting][]
- [HIFIS Service Overview][helm-cloud]


<!-- Links -->

[helm-cloud]: https://cloud.helmholtz.de/services
[test-indico]: https://events.hifis.net/category/5/
[indico.github]: https://github.com/indico/indico
[indico]: https://getindico.io/
[events.hifis.net]: https://events.hifis.net
[learn.getindico.io]: https://learn.getindico.io/
[coc-helpdesk]: mailto://support@hifis.net?subject=Code-of-Conduct%20violation
[coc-carp-doc]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html
[coc-carp]: https://goo.gl/forms/KoUfO53Za3apOuOK2
[indico-doc]: https://learn.getindico.io/

<!-- course related links -->
[survey]: (https://events.hifis.net/event/332/surveys/73?token=6ae789df-5773-4302-aae6-f888899c2dac)



