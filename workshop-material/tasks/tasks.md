---
title: Tasks
---

## Task 1: Create an event page {#task01}


??? task
    
    1. Select the *Conference* type
    2. Enter the *Title* "Getting started with Git - "random-identifier"
    3. Enter a *Start/End* Date: one month from now
    
??? example "Hint"

    ![Indico type](../images/indico_type.svg)

    -----

    ![Indico course](../images/indico_new-course02.png)


## Task 2: Create a landing page {#task02}

??? task
    
    Generate the following landing page for your event.

    -----
    - **Title**
        
        Getting started with Git
        
    - **General Information**
    
        |                     |                          |
        | ----                | ----                     |
        | Language            | English                  |
        | Cost                | Free of charge           |
        | Level               | Beginner                 |
        | Registration Period | YYYY-MM-DD to YYYY-MM-DD |
    
     - **Workshop Content**
        
        Working with a command-line interface (Shell) as a foundation for the following tools employing Git as a version control system (VCS) for managing changes while working with files. The chapter will be taught during the course:

        - Introduction to version control
        - Git setup
        - Basic local Git workflow
        - Branching and merging
        - Resolving Conflicts
    

    - **Registration**
        
        To register, please log in via Helmholtz AAI. Once you have registered, you should receive a confirmation e-mail. If you receive no such e-mail (and it is not in the spam folder), don't hesitate to contact the organizer.
    
   
    - **Preparations**
    
        Participants are asked to make sure they have the necessary rights to install software on their computers. For this workshop two tools are required:
        
        - A Unix-compatible command-line interface (Shell for short)
        - The git tool
        
        You can find detailed installation instructions for each of these tools [**here**][installation-guide].
    
    -----

    - In addition add the keywords: `git` and `gitlab`


??? example "Hint"
    
    Example html source:

    ``` html
    <h2>
        <span style="color:hsl(30,75%,60%);">General Information</span>
    </h2>
    <figure class="table">
        <table>
            <tbody>
                <tr>
                    <td style="height:30px;padding:3px;width:150px;">
                        <span style="color:hsl(0,0%,30%);"><strong>Language</strong></span>
                    </td>
                    <td style="width:200px;">
                        <span style="color:hsl(0,0%,30%);">English</span>
                    </td>
                </tr>
                <tr>
                    <td style="height:30px;padding:3px;width:150px;">
                        <span style="color:hsl(0,0%,30%);"><strong>Cost</strong></span>
                    </td>
                    <td style="width:200px;">
                        <span style="color:hsl(0,0%,30%);">Free</span>
                    </td>
                </tr>
                <tr>
                    <td style="height:30px;padding:3px;width:150px;">
                        <span style="color:hsl(0,0%,30%);"><strong>Level</strong></span>
                    </td>
                    <td style="width:200px;">
                        <span style="color:hsl(0,0%,30%);">Beginner</span>
                    </td>
                </tr>
                <tr>
                    <td style="height:30px;padding:3px;width:150px;">
                        <span style="color:#2980b9;"><strong>Registration</strong></span>
                    </td>
                    <td style="width:200px;">
                        <span style="color:#2980b9;"><strong>14.03.2023</strong> to <strong>14.03.2023</strong></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </figure>
    <h2>
        <span style="color:hsl(30,75%,60%);">Workshop Content</span>
    </h2>
    <p>
        <span style="background-color:rgb(255,255,255);color:hsl(0,0%,30%);">Working with a command-line interface (Shell) as a foundation for the following tools employing Git as a version control system (VCS) for managing changes while working with files. The chapter will be taught during the course:</span>
    </p>
    <ul>
        <li>
            <span style="color:hsl(0,0%,30%);">Introduction to version control</span>
        </li>
        <li>
            <span style="color:hsl(0,0%,30%);">Git setup</span>
        </li>
        <li>
            <span style="color:hsl(0,0%,30%);">Basic local Git workflow</span>
        </li>
        <li>
            <span style="color:hsl(0,0%,30%);">Branching and merging</span>
        </li>
        <li>
            <span style="color:hsl(0,0%,30%);">Resolving Conflicts</span>
        </li>
        <li>
            <span style="color:hsl(0,0%,30%);">Working with Git repositories</span>
        </li>
    </ul>
    <h2>
        <span style="color:hsl(30,75%,60%);">Registration</span>
    </h2>
    <p>
        <span style="color:hsl(0,0%,30%);">To register, please log in via Helmholtz AAI. Once you have registered, you should receive a confirmation e-mail. If you receive no such e-mail (and it is not in the spam folder), don't hesitate to contact the organizer.</span>
    </p>
    <h2>
        <span style="color:hsl(30,75%,60%);">Preparations</span>
    </h2>
    <p>
        <span style="color:hsl(0,0%,30%);">Participants are asked to make sure they have the necessary rights to install software on their computers. For this workshop, two tools are required:</span>
    </p>
    <ul>
        <li>
            <span style="color:hsl(0,0%,30%);">A Unix-compatible command-line interface (Shell for short)</span>
        </li>
        <li>
            <span style="color:hsl(0,0%,30%);">The git tool</span>
        </li>
    </ul>
    <p>
        <span style="color:hsl(0,0%,30%);">You can find detailed installation instructions for each of these tools </span><a href="hifis.net"><span style="color:#2980b9;"><strong>here</strong></span></a><span style="color:hsl(0,0%,30%);">.</span>&nbsp;&nbsp;&nbsp;
        <br>
        &nbsp;
    </p>
    ```


<!---- Links ---->
[installation-guide]: https://hifis.net
[installation-guide-old]: https://codimd.desy.de/5HWWnHHSTYSYqxb4oPlf_A#


## Task 3: Create a timetable {#task03}

??? task

    Generate the following timetable for your course.

    -----
    
    **Introduction (15')**
    
    !!! info
        - Welcome!
        - Getting to know each other
        - How to course works?
    
    **Session Bash (60')**
    
    1. Introducing the Shell (20')
    2. Navigating Files and Directories (20')
    3. Working With Files and Directories (20')
    
    **Coffee Break (15')**
    
    **Session Git 1 (90')**
    
    1. Getting started (30')
    1. Tracking and Exploring Changes (60')
    
    **Lunch (60')**
    
    **Session Git 2 (90')**
    
    1. Collaborating with other users(45')
    1. Conflicts (45')
    
    **Cookie Break (15')**
    
    **Session GitLab (45')**
    
    1. Remotes in GitLab (30')
    1. Open Science (15')
    
    **Questions and Closing (15')**
    
    - Questions 
    - AOB


??? example "Hint"

    ![Indico timetable](../images/indico_timetable-example.png){width=75%}


## Task 4: Setting up a registration form {#task04}

??? task
    
    - Create two registration forms:
    
        - **Participants**" and 
        - "**Waiting List**".
    
    - `Create form` settings (same as `General settings`) for this course:
        
        - Titles "**Participants**" and "**Waiting List**"
        - Only logged-in users: Yes
        - Limit registration: 20 (Participation List only)
    
        ![Indico registration01](../images/indico_registration01.png){: width=70%}
    
    - Registration Manage Overview
        
        ![Indico registration01](../images/indico_registration02.png){: width=70%}
    
    - You can modify the following:
        
        - **Registration Form** (recommended)
        - Scheduler for the visibility of the registration forms
        - and the 'General settings' (initial settings)


