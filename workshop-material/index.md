---
tags: #indico
title: Introduction
---


## Overview

<!-- Motivation -->
!!! note "What is this workshop about?"
    We are here to learn something about how to manage an event via the web tool [Indico][indico]. The goal is to enable you to set up a **workshop**.
    
    In the next 90 minutes we will learn how to:
    
    - [Create an Event Page](episodes/create-event-page.md)
    - [Generate Timetables](episodes/generating-timetable.md)
    - [Set up Registrations](episodes/registration.md)
    - [Managing Access and Visibility](episodes/access.md)
    <!-- - [Create a Survey](episodes/survey.md) -->
    <!-- - [Managing Conference Contributions](episodes/conference.md) -->


## Requirements

!!! warning "Before we get started!"
    <p style="margin-bottom:1em"></p>
    Most actions in Indico require you to be logged in.
    During your first login an account will be set up and linked to the Helmholtz AAI.
    
    <p style="margin-bottom:1.5em"></p>

    ![Indico Login](./images/indico_login.svg)


## Helpful Information and Links

<!-- What is Indico -->

[Indico][indico] is a self-hosted web-based event management tool actively developed by CERN.

The Indico instance we are talking about in this workshop is a hosted service by HIFIS, a Helmholtz initiative.

!!! info "Link to Helmholtz Indico: >> [events.hifis.net] <<"

Further information can be found in:

- User documentation can be found on [getindico.io][getindico]
- Indico code repository on [github][indico.github]

-----

Citation notes:

- Cited text are taken from the page [learn.getindico.io]

<!---- Links and Literature ---->
[gather]: https://www.gather.town/
[hedgedoc-pad]: https://notes.desy.de/
[events.hifis.net]: https://events.hifis.net
[indico]: https://getindico.io/
[getindico]: https://learn.getindico.io/
[AAI]: https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html
[indico.github]: https://github.com/indico/indico
[learn.getindico.io]: https://learn.getindico.io/

