---
title: About Us
---

----

<figure markdown="1" style="width:70%">
<a href="https://hifis.net/">
![HIFIS Logo](./images/HIFIS_blue.svg)
<center>
![HIFIS Claim](./images/HIFIS_claim.svg){: style="width:75%"} 
</center>
</a>
</figure>

----

## Mission Statement

HIFIS stands for Helmholtz Federated IT Services.

The top priority of Helmholtz research is cross-centre and international cooperation and common access to data treasure and services.
At the same time, the significance of sustainable software development for the research process is recognised.

**HIFIS builds and sustains an excellent IT infrastructure connecting all Helmholtz research fields and centres.**

Further details can be found on the [HIFIS website][mission].

The overall mission of HIFIS Consulting is adopted from the [HGF software policy][policy] to the specific needs.
It ensures efficient and effective sustainable state-of-the-art software development and data science.
The HGF software policy defines the scope and serves as a guiding framework to identify and prioritise topics that are addressed by the consulting service.

## Feedback

Please [**contact us**](mailto:support@hifis.net) if you are missing any content or if you have general comments.

<!-- LINKS -->
[hifis]: https://hifis.net/
[policy]: https://doi.org/10.48440/os.helmholtz.041
[mission]: https://hifis.net/mission.html
[helpful]: consulting_resources.md
[consulting-pad]: resources/template-consulting-pad.md
[appointment]: resources/template-welcome-note.md
[invite]: resources/template-invite-email.md
[stackoverflow]: https://stackoverflow.com/
[hifis-training]: https://www.hifis.net/services/software/training 
