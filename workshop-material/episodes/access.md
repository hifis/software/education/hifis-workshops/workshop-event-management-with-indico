---
title: "&#10115; Access and Visibility"
---

## Event Protection Settings

- Change accessibility (Public/Protected)
- Change visibility (From everywhere/Invisible)
- Add permission to managers, speakers and Bots
- Manage and create new roles

![Indico protection settings](../images/indico_protection.svg)

!!! info ""
    <center>
    :warning: **Be aware to save your settings on the bottom** :warning:
    </center>

