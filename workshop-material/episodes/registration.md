---
title: "&#10114; Set up Registration"
---

In this section we will talk about creation and managment of your registration or multiple registration forms.

![Indico registration](../images/indico_registration.svg)

!!! info
    The **'Affiliation'** field is not provided by the corresponding centers consistenly therefore it can not be used to determine affiliations correctly, currently.

!!! excercise 
    Do [Task 4](../tasks/tasks.md#task04).

