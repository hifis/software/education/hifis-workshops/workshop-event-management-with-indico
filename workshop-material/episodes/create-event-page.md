---
title: "&#10112; Creating an Event Page"
---

## Getting started 

Events can be of certain types. You can switch the event type any time after the event creation.

!!! attention
    If the new event type has less features, than the old one, the missing features get deactivated and the according configuration are stored in the background.

**Available Event types**

- Lectures
    
    > can be seen as a single session, with one or more speakers

- Meetings
    
    > are usually composed of multiple sessions. They have a timetable. Contributions (talks) and can be organized in sessions
    
- Conferences
    
    > generally last multiple days and have the advantage over meetings of allowing multiple sessions to happen in parallel. They also offer flexible abstract and paper reviewing/editing workflows

![Indico type](../images/indico_type.svg)

All of these meeting types offer a set of common features, such as:

- Registration
- Management of participants
- Surveys
- Integration with collaborative tools
- Event reminders

| Lecture                                          | Meeting                                          | Conference                                          |
| :----:                                             | :----:                                             | :----:                                                
| ![Indico Lecture Menu](../images/indico_lecture.png){: align="top"} | ![Indico Meeting Menu](../images/indico_meeting.png){: align="top"} | ![Indico Conference Menu](../images/indico_conference.png){: align="top"} |

We recommend to use the **Conference** type as default.

!!! excercise 
    Do [Task 1](../tasks/tasks.md#task01).


## Event page

The event page is the landing page for the event and should hold all needed informations. It is managed under `Settings`. The following information should be included:

- Title
- Desciption
    - General Information
	- Workshop Content
	- Registration
	- Preparations
- Schedule
- Contact
- Keywords
    
    > Keywords can be added by typing into the appropriate input field and pressing `,` to separate keywords.
    
    ![](../images/indico_keywords.png){style="transform: scale(0.75);"}

- Labels

![Indico Edit Mode](../images/indico_edit-mode.png)

!!! excercise 
    Do [Task 2](../tasks/tasks.md#task02).

