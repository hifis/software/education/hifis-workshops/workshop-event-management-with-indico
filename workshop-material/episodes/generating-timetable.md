---
title: "&#10113; Generating a Timetable"
---

A timetable can consist of

- Breaks
- Contributions
- Sessions (Collection of Contributions)

!!! attention
    
    Sessions are organized in so-called session blocks, which are a type of organizing unit. Imagine you want to group your workshop or conference in specific topics or rooms, like Material, Energy, Health, etc. Within those topic blocks, you can now define the sessions which will take place. 



The time frame of the timetable depends on the initial defined time range!

![Indico timetable](../images/indico_timetable.svg)

Start out to generate the sessions blocks, contributions, and breaks. The session blocks consist themselves of contributions and breaks. The times do not need to fit perfectly into the schedule. 

!!! excercise 
    Do [Task 3](../tasks/tasks.md#task03).

